package controllers;

import models.Task;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.*;

import views.html.*;

import javax.inject.Inject;
import java.util.List;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends Controller {

    @Inject
    play.data.FormFactory formFactory;

    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */
    public Result index() {

        // Find all tasks
        List<Task> tasks = Task.find.all();

        return ok(index.render("Your new application is ready.",tasks));
    }


    public  Result add() {
        Form<Task> form= formFactory.form(Task.class).bindFromRequest();
        form.get().save();
        return redirect(routes.HomeController.index());
    }

    public Result delete(Long id) {
        Task.find.ref(id).delete();

        return redirect(routes.HomeController.index());
    }

    public Result update(long id) {
        Task taskOld = Task.find.ref(id);
        DynamicForm form = Form.form().bindFromRequest();

        String newName = form.get("name");

        taskOld.name = newName;

        taskOld.update();

        return redirect(routes.HomeController.index());
    }


}
